package com.config;

import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.InetAddress;

@Configuration
public class ElsticsearchConfig {
    @Value("${spring.data.elasticsearch.cluster-name}")
  private  String clusterName;
    @Value("${spring.data.elasticsearch.cluster-nodes}")
  private  String  clusterNodes;
    @Value("${spring.data.elasticsearch.cluster-nodes.port}")
  private  Integer  clusterNodesPort;

    private static final Logger logger = LoggerFactory.getLogger(ElsticsearchConfig.class);

    @Bean
 public  TransportClient transportClient() {
        try {
            Settings settings = Settings.builder().put("cluster.name", clusterName).build();
            TransportClient  client = new PreBuiltTransportClient(settings)
                    .addTransportAddress(new TransportAddress(InetAddress.getByName(clusterNodes), clusterNodesPort));
            logger.error("数据库连接成功");
            return client;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("elstic数据库",e);
            return null;
        }
  }


}
