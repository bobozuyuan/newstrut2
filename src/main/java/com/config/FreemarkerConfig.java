/**
 * Copyright 2018 贝壳开源 http://www.beikbank.com
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.config;

//import com.beikbank.modules.sys.shiro.ShiroTag;
//import com.beikbank.modules.sys.shiro.ShiroTag;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import com.migo.shiro.VelocityShiro;

/**
 * Freemarker配置
 *
 * @author Mark sunlightcs@gmail.com
 * @since 3.0.0 2017-09-28
 */
@Configuration
public class FreemarkerConfig {

    @Value("${spring.freemarker.template-loader-path}")//=classpath:/templates,WEB-INF/page
    private String templateLoaderPath;
    @Value("${spring.freemarker.cache}")
    private String  cache;
    @Value("${spring.freemarker.charset}")
    private String  charset;
    @Value("${spring.freemarker.check-template-location}")
    private String  checkTemplate;
    @Value("${spring.freemarker.content-type}")
    private String  contentType;
    @Value("${spring.freemarker.expose-request-attributes}")
    private String  exposeRequestAttributes;
    @Value("${spring.freemarker.expose-session-attributes}")
    private String  exposeSessionAttributes;
    @Value("${spring.freemarker.request-context-attribute}")
    private String  requestContextAttribute;
    @Value("${spring.freemarker.suffix}")
    private String  suffix;


    @Bean
    public FreeMarkerConfigurer freeMarkerConfigurer(VelocityShiro shiroTag){
        FreeMarkerConfigurer   configurer= new FreeMarkerConfigurer();
        //configurer.setTemplateLoaderPath(templateLoaderPath);
        configurer.setTemplateLoaderPaths("classpath:/templates","WEB-INF/page");
        Map<String, Object> variables = new HashMap<>(1);
        variables.put("shiro", shiroTag);
        configurer.setFreemarkerVariables(variables);
        Properties settings = new Properties();
            settings.setProperty("default_encoding", "utf-8");
            settings.setProperty("number_format", "0.##");
        configurer.setFreemarkerSettings(settings);
        return configurer;
    }

}