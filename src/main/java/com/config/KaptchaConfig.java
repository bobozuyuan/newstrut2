package com.config;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

@Configuration
public class KaptchaConfig {

    @Bean
    public DefaultKaptcha producer(){
        DefaultKaptcha   defaultKaptcha=new DefaultKaptcha();
        Properties p=new Properties();
              p.setProperty("kaptcha.border","no");
              p.setProperty("kaptcha.textproducer.font.color","black");
              p.setProperty("kaptcha.textproducer.char.space","5");
              Config config=new Config(p);
              defaultKaptcha.setConfig(config);
        return       defaultKaptcha;
    }



}
