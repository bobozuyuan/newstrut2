package com.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import javax.sql.DataSource;
import java.util.Properties;
@Configuration
public class QuartzSchedulerConfig {

    @Bean
    public SchedulerFactoryBean  scheduler(DataSource dataSource){
            SchedulerFactoryBean s=new  SchedulerFactoryBean();
                    s.setDataSource(dataSource);

                 Properties quartzProperties=new Properties();
                      quartzProperties.setProperty("org.quartz.scheduler.instanceName","MigoScheduler");
                      quartzProperties.setProperty("org.quartz.scheduler.instanceId","AUTO");
                      quartzProperties.setProperty("org.quartz.threadPool.class","org.quartz.simpl.SimpleThreadPool");
                      quartzProperties.setProperty("org.quartz.threadPool.threadCount","20");
                      quartzProperties.setProperty("org.quartz.threadPool.threadPriority","5");
                    //<!-- JobStore 配置 -->
                      quartzProperties.setProperty("org.quartz.jobStore.class","org.quartz.impl.jdbcjobstore.JobStoreTX");
                    //<!-- 集群配置 -->
                      quartzProperties.setProperty("org.quartz.jobStore.isClustered","true");
                      quartzProperties.setProperty("org.quartz.jobStore.clusterCheckinInterval","15000");
                      quartzProperties.setProperty("org.quartz.jobStore.maxMisfiresToHandleAtATime","1");
                      quartzProperties.setProperty("org.quartz.jobStore.misfireThreshold","12000");
                    //<!-- 表前缀 -->
                      quartzProperties.setProperty("org.quartz.jobStore.tablePrefix","QRTZ_");
//                quartzProperties.setProperty("schedulerName","MigoScheduler");
//           //<!--延时启动 -->
//                quartzProperties.setProperty("startupDelay","30" );
//                quartzProperties.setProperty("applicationContextSchedulerContextKey", "applicationContextKey");
//           //<!--可选，QuartzScheduler 启动时更新己存在的Job，这样就不用每次修改targetObject后删除qrtz_job_details表对应记录了 -->
//                quartzProperties.setProperty("overwriteExistingJobs","true");
//            //<!-- 设置自动启动   默认为true -->
//                quartzProperties.setProperty("autoStartup","true");
                    s.setQuartzProperties(quartzProperties);
                    s.setSchedulerName("MigoScheduler");
                    s.setStartupDelay(30);
                    s.setApplicationContextSchedulerContextKey("applicationContextKey");
                    s.setOverwriteExistingJobs(true);
                    s.setAutoStartup(true);
                    return s;
    }



}
