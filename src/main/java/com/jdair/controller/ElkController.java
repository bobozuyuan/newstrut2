package com.jdair.controller;


import com.alibaba.fastjson.JSON;
import com.jdair.jpadao.ZabbixRepository;
import com.jdair.jpaentity.Person;
import com.jdair.service.ElkService;
import com.jdair.service.PersonService;
import com.migo.annotation.IgnoreAuth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class ElkController {
     @Autowired
     ElkService      elkService ;
     @Autowired
     PersonService   personService;
     @Autowired
     ZabbixRepository zabbixRepository;

     @IgnoreAuth
     //@RequestMapping("/matchQuery")
     @RequestMapping(value = "/matchQuery",produces="application/json;charset=UTF-8")
     public  String  matchQuery(){
         List<Person>  personList= personService.findByName("yb-s");
        System.out.println(JSON.toJSONString(personList));
         return  elkService.matchQuery();
     }


    @RequestMapping("/matchQueryjson")
    public Date matchQueryjson(){
        return  new Date();
    }

    @IgnoreAuth
    @RequestMapping("/getelkinfo")
    public List getelkinfo(){
          return zabbixRepository.getZabbixInfo();
    }

    @IgnoreAuth
    @RequestMapping("/getUsrgrpList")
    public List getUsrgrpList(){
          return zabbixRepository.getUsrgrpList();
    }


}
