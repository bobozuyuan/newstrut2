package com.jdair.jpadao;

import com.jdair.jpaentity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by zhangh-ag on 2017-06-12.
 */
public interface PersonRepository extends JpaRepository<Person, String> {

    @Query("from Person p where p.name =:name")
    List<Person> findByName(@Param(value = "name") String name);

    @Query("from Person p where p.loginName =:name")
    Person getPassword(@Param(value = "name") String name);
}