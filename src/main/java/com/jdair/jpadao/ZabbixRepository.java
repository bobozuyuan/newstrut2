package com.jdair.jpadao;

import com.jdair.jpaentity.Person;
import com.jdair.jpaentity.UsrgrpEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class ZabbixRepository {

@Autowired
@PersistenceContext
private EntityManager entityManager;


//
public List  getZabbixInfo(){

     String sql="SELECT b.usrgrpid,b.name,a.userid,c.sendto FROM users_groups a,usrgrp b,media c\n" +
                 "WHERE a.usrgrpid=b.usrgrpid\n" +
                 "AND c.userid=a.userid\n" +
                 "AND c.mediatypeid in(?1,?2)\n" +
                 "ORDER BY b.usrgrpid";
              Query query =    entityManager.createNativeQuery(sql);
                    query.setParameter(1,5);
                    query.setParameter(2,3);
               List objecArraytList = query.getResultList();
             for(int i=0;i<objecArraytList.size();i++) {
                    Object[] obj = (Object[]) objecArraytList.get(i);
                    //使用obj[0],obj[1],obj[2]...取出属性　
                    System.out.println("id = " + obj[0]);
                    System.out.println("name = " + obj[1]);
                    System.out.println("age = " + obj[2]);
             }
             return objecArraytList;
}
//分组列表
    public List<UsrgrpEntity> getUsrgrpList(){

           String jpql                   = "from UsrgrpEntity";
           TypedQuery query              =  entityManager.createQuery(jpql,UsrgrpEntity.class);
           List<UsrgrpEntity>  queryList = query.getResultList();
           return queryList;
    }

}
