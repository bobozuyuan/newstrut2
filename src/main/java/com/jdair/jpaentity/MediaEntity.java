package com.jdair.jpaentity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "media", schema = "zabbix", catalog = "")
public class MediaEntity {
    private long mediaid;
    private String sendto;
    private int active;
    private int severity;
    private String period;

    @Id
    @Column(name = "mediaid")
    public long getMediaid() {
        return mediaid;
    }

    public void setMediaid(long mediaid) {
        this.mediaid = mediaid;
    }

    @Basic
    @Column(name = "sendto")
    public String getSendto() {
        return sendto;
    }

    public void setSendto(String sendto) {
        this.sendto = sendto;
    }

    @Basic
    @Column(name = "active")
    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    @Basic
    @Column(name = "severity")
    public int getSeverity() {
        return severity;
    }

    public void setSeverity(int severity) {
        this.severity = severity;
    }

    @Basic
    @Column(name = "period")
    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MediaEntity that = (MediaEntity) o;
        return mediaid == that.mediaid &&
                active == that.active &&
                severity == that.severity &&
                Objects.equals(sendto, that.sendto) &&
                Objects.equals(period, that.period);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mediaid, sendto, active, severity, period);
    }
}
