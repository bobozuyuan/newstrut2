package com.jdair.jpaentity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "media_type", schema = "zabbix", catalog = "")
public class MediaTypeEntity {
    private long mediatypeid;
    private int type;
    private String description;
    private String smtpServer;
    private String smtpHelo;
    private String smtpEmail;
    private String execPath;
    private String gsmModem;
    private String username;
    private String passwd;
    private int status;

    @Id
    @Column(name = "mediatypeid")
    public long getMediatypeid() {
        return mediatypeid;
    }

    public void setMediatypeid(long mediatypeid) {
        this.mediatypeid = mediatypeid;
    }

    @Basic
    @Column(name = "type")
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "smtp_server")
    public String getSmtpServer() {
        return smtpServer;
    }

    public void setSmtpServer(String smtpServer) {
        this.smtpServer = smtpServer;
    }

    @Basic
    @Column(name = "smtp_helo")
    public String getSmtpHelo() {
        return smtpHelo;
    }

    public void setSmtpHelo(String smtpHelo) {
        this.smtpHelo = smtpHelo;
    }

    @Basic
    @Column(name = "smtp_email")
    public String getSmtpEmail() {
        return smtpEmail;
    }

    public void setSmtpEmail(String smtpEmail) {
        this.smtpEmail = smtpEmail;
    }

    @Basic
    @Column(name = "exec_path")
    public String getExecPath() {
        return execPath;
    }

    public void setExecPath(String execPath) {
        this.execPath = execPath;
    }

    @Basic
    @Column(name = "gsm_modem")
    public String getGsmModem() {
        return gsmModem;
    }

    public void setGsmModem(String gsmModem) {
        this.gsmModem = gsmModem;
    }

    @Basic
    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "passwd")
    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    @Basic
    @Column(name = "status")
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MediaTypeEntity that = (MediaTypeEntity) o;
        return mediatypeid == that.mediatypeid &&
                type == that.type &&
                status == that.status &&
                Objects.equals(description, that.description) &&
                Objects.equals(smtpServer, that.smtpServer) &&
                Objects.equals(smtpHelo, that.smtpHelo) &&
                Objects.equals(smtpEmail, that.smtpEmail) &&
                Objects.equals(execPath, that.execPath) &&
                Objects.equals(gsmModem, that.gsmModem) &&
                Objects.equals(username, that.username) &&
                Objects.equals(passwd, that.passwd);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mediatypeid, type, description, smtpServer, smtpHelo, smtpEmail, execPath, gsmModem, username, passwd, status);
    }
}
