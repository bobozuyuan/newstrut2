package com.jdair.jpaentity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "t_person_role")
public class PersonRole {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;
    private String role;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }


    @OneToMany
   // @JoinColumn(name = "role_id", nullable = false)
    private Set<Person> personRecords;
}
