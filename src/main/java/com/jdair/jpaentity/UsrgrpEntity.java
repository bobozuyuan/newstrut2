package com.jdair.jpaentity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "usrgrp", schema = "zabbix", catalog = "")
public class UsrgrpEntity {
    private long usrgrpid;
    private String name;
    private int guiAccess;
    private int usersStatus;
    private int debugMode;

    @Id
    @Column(name = "usrgrpid")
    public long getUsrgrpid() {
        return usrgrpid;
    }

    public void setUsrgrpid(long usrgrpid) {
        this.usrgrpid = usrgrpid;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "gui_access")
    public int getGuiAccess() {
        return guiAccess;
    }

    public void setGuiAccess(int guiAccess) {
        this.guiAccess = guiAccess;
    }

    @Basic
    @Column(name = "users_status")
    public int getUsersStatus() {
        return usersStatus;
    }

    public void setUsersStatus(int usersStatus) {
        this.usersStatus = usersStatus;
    }

    @Basic
    @Column(name = "debug_mode")
    public int getDebugMode() {
        return debugMode;
    }

    public void setDebugMode(int debugMode) {
        this.debugMode = debugMode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsrgrpEntity that = (UsrgrpEntity) o;
        return usrgrpid == that.usrgrpid &&
                guiAccess == that.guiAccess &&
                usersStatus == that.usersStatus &&
                debugMode == that.debugMode &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(usrgrpid, name, guiAccess, usersStatus, debugMode);
    }
}
