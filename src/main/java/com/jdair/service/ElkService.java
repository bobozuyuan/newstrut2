package com.jdair.service;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ElkService {

    @Autowired
   private TransportClient transportClient;

    public  String   matchQuery() {
        SearchResponse res = null;
        QueryBuilder qb = QueryBuilders.matchQuery("geoip.ip", "223.104.1.245");

        res = transportClient.prepareSearch("logstash-nginx_itdc")
                //.setTypes("geoip")
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(qb)
                .setFrom(0)
                .setSize(5)
                .execute().actionGet();

        StringBuilder  sb=new StringBuilder();
        for (SearchHit hit: res.getHits().getHits()){
            System.out.println(hit.getSourceAsString());
            sb.append(hit.getSourceAsString());
        }

        // on shutdown
       //  transportClient.close();
        return sb.toString();
    }

    public  void matchQuery2() {
        SearchResponse res = null;
        QueryBuilder   qb  = QueryBuilders.matchQuery("logmessage", "田帅");

                     res = transportClient.prepareSearch("logstash-tomcat_mini")
                //.setTypes("geoip")
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(qb)
                .setFrom(0)
                //.setSize(5)
                .execute().actionGet();
        for (SearchHit hit: res.getHits().getHits()){
            System.out.println(hit.getSourceAsString());
        }

        // on shutdown
        // client.close();
    }

}
