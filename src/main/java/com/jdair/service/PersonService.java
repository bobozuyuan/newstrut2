package com.jdair.service;

import com.jdair.jpaentity.Person;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface PersonService {
    //@Transactional(readOnly = false,isolation = Isolation.READ_COMMITTED)
    List<Person> findByName(String name);


    String getPersionPassWord(String name);

    String getRole(String username);

    String getPassword(String username);
}
