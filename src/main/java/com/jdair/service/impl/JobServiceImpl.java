package com.jdair.service.impl;

import com.jdair.service.JobService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service("testTask")
public class JobServiceImpl implements JobService {

    private static final Logger logger = LoggerFactory.getLogger(JobServiceImpl.class);

    @Override
    public  void test1(String migo){
        logger.error("执行job.test1="+migo);
    }

    @Override
    public  void test2(){
        logger.error("执行job.test2");
    }
}
