package com.jdair.service.impl;

import com.jdair.jpadao.PersonRepository;
import com.jdair.jpaentity.Person;
import com.jdair.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {


    @Autowired
    private PersonRepository personRepository;

    @Override
   // @Transactional(readOnly = false,isolation = Isolation.READ_COMMITTED)
    public List<Person> findByName(String name){
        return personRepository.findByName(name);
    }

    @Override
    public String getPersionPassWord(String name) {
        Person person =   personRepository.getPassword(name);
        return person.getPassWord();
    }
    @Override
    public String getPassword(String username) {
        return getPersionPassWord(username);
    }

    @Override
    public String getRole(String username) {
        Person person =   personRepository.getPassword(username);
        return person.getRole().getRole();


    }


}
