package com.jdair.elk;

import com.alibaba.fastjson.JSON;
import org.elasticsearch.action.admin.cluster.node.info.NodeInfo;
import org.elasticsearch.action.admin.cluster.node.info.NodesInfoResponse;
import org.elasticsearch.action.admin.indices.stats.CommonStats;
import org.elasticsearch.action.admin.indices.stats.IndexStats;
import org.elasticsearch.action.admin.indices.stats.IndicesStatsResponse;
import org.elasticsearch.action.admin.indices.stats.ShardStats;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.IndicesAdminClient;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.cluster.node.DiscoveryNode;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RegexpQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.junit.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.util.List;
import java.util.Map;


public class ElkmainJunitTest {

	private static final Logger logger = LoggerFactory.getLogger(ElkmainJunitTest.class);

    private  TransportClient client;

    @Before
    public   void getClient() {
        // TODO Auto-generated method stub
         try {
              Settings settings = Settings.builder().put("cluster.name", "JdES").build();
              client = new PreBuiltTransportClient(settings)
                      .addTransportAddress(new TransportAddress(InetAddress.getByName("10.72.84.216"), 9300));  
              System.out.println("success to connect escluster");
             // createDate(client);
             // GetResponse response = client.prepareGet("accounts", "person", "1").execute().actionGet();  
             //System.out.println(response.getSource());  
             //client.close();
          } catch (Exception e) {  
              e.printStackTrace();  
          }
    }

	/**
	 * 关闭连接
	 */
	@After
	public  void close() {
		client.close();
	}

	//@Test
   public  void getDataByID(String id) {
        // TODO Auto-generated method stub
         try {  
  
              GetResponse response = client.prepareGet("accounts", "person", id).execute().actionGet();  
  
              System.out.println(response.getSource());  

              //client.close();  
          } catch (Exception e) {  
              e.printStackTrace();  
          }  
    }

	@Test
	public  void matchQuery() {
		SearchResponse res = null;
		QueryBuilder qb = QueryBuilders.matchQuery("geoip.ip", "223.104.1.245");
		
		res = client.prepareSearch("logstash-nginx_itdc")
				//.setTypes("geoip")
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
				.setQuery(qb)
				.setFrom(0)
				.setSize(5)
				.execute().actionGet();
		for (SearchHit hit: res.getHits().getHits()){
			System.out.println(hit.getSourceAsString());
		}
		
		// on shutdown
		client.close();
	}

	@Test
	public  void matchQuery2() {
		SearchResponse res = null;
		QueryBuilder qb = QueryBuilders.matchQuery("logmessage", "田帅分");

		//QueryBuilder qb = QueryBuilders.matchPhraseQuery("logmessage", "田帅");
		System.out.println(qb.toString());

		res = client.prepareSearch("logstash-tomcat_mini")
				//.setTypes("geoip")
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
				.setQuery(qb)
				.setFrom(0)
				.setSize(500)
				.execute().actionGet();
		//System.out.println(res.toString());
		for (SearchHit hit: res.getHits().getHits()){
			System.out.println(hit.getSourceAsString());
		}
		
		// on shutdown
		client.close();
	}

	//
	@Test
	public  void matchQueryRegexpQueryBuilder() {
		SearchResponse res = null;

	//	RegexpQueryBuilder qb =new RegexpQueryBuilder("logmessage", "田帅");
		QueryBuilder qb = QueryBuilders.regexpQuery("logmessage.keyword", ".*田帅.*");

		SearchRequestBuilder  srb= client.prepareSearch("logstash-tomcat_mini")
				//.setTypes("geoip")
				//.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
				.setQuery(qb)
				.setFrom(0)
				.setSize(500);


		System.out.println(srb.toString());
		res=		srb.execute().actionGet();
		System.out.println(res.toString());
		for (SearchHit hit: res.getHits().getHits()){
			System.out.println(hit.getSourceAsString());
		}

		// on shutdown
		client.close();
	}


	//得到索引列表

	/**
	 * 索引统计
	 *
	 * @param index
	 * 参考
	 * http://www.hillfly.com/2016/131.html
	 */
	//@Test
	public  void indexStats(String index) {
		IndicesAdminClient indicesAdminClient = client.admin()
				.indices();
		IndicesStatsResponse response = indicesAdminClient.prepareStats(index)
				.all().get();
		indicesAdminClient.prepareStats().execute().actionGet();
		ShardStats[] shardStatsArray = response.getShards();
		for (ShardStats shardStats : shardStatsArray) {
			logger.info("shardStats {}", shardStats.toString());
		}
		Map<String, IndexStats> indexStatsMap = response.getIndices();
		for (String key : indexStatsMap.keySet()) {
			logger.info("indexStats {}", indexStatsMap.get(key));
		}
		CommonStats commonStats = response.getTotal();
		logger.info("total commonStats {}", commonStats.toString());
		commonStats = response.getPrimaries();
		logger.info("primaries commonStats {}", commonStats.toString());
	}

	/**
	 * 获取ES集群信息
	 */
	@Test
	public  void getNodes() {
			NodesInfoResponse resp = client.admin().cluster().prepareNodesInfo().execute().actionGet();
			//NodeInfo[] nodes =
		    //resp.
		List<NodeInfo> nodes =	resp.getNodes();
			for(NodeInfo nodeInfo : nodes){
				DiscoveryNode node = nodeInfo.getNode();
				System.out.println("version:" + node.getVersion());
				System.out.println("address:" + node.getHostAddress());
				System.out.println("nodeId:" + node.getId());
				System.out.println("nodeInfo:" + JSON.toJSONString(node));
			}
	}

/**
 * 但是可以通过别的 API 间接获取到：
 * 比如通过以下 HTTP API：http://10.1.109.77:9200/_all；
 * 对应 JAVA API 为： GetIndexResponse resp = client.admin().indices().prepareGetIndex().execute().actionGet();；
 * 这里我就不贴运行结果了，大家可以自己试试，可以发现返回的信息很多，除了 indexList 外，还有 mappings 等信息。
 *
 * 但是我更偏向使用另一个 API 来获取索引列表：
 * HTTP： http://10.1.109.77:9200/_stats；
 * JAVA-API：IndicesStatsResponse resp = client.admin().indices().prepareStats().execute().actionGet();
 * 为什么偏向使用这个？因为这个接口还有返回索引的存储信息，包括缓存大小、索引大小等等，方便我们作为优化参考。
 *
 * 这里偏个题，如果需要通过这个 API 获取某个索引的存储信息，比如我要获取索引名为 HILL-INDEX-TEST1 的存储信息，可以这么做：
 * HTTP： http://10.1.109.77:9200/HILL-INDEX-TEST1/_stats;
 * JAVA-API：
 *
 * IndicesStatsResponse resp = client.admin().indices().prepareStats()
 *                                           .setIndices("HILL-INDEX-TEST1")
 *                                           .execute().actionGet();
 * 另外 setIndices 还支持通配符查询，比如我要查询以 HILL - 开头的所有索引的存储信息：setIndices("HILL-*")。
 * **/
@Test
public void getindex(){
	IndicesStatsResponse resp = client.admin().indices().prepareStats().execute().actionGet();
	Map<String, IndexStats>   indexmapping =  resp.getIndices();
	for(String indexName :  indexmapping.keySet() ){
          logger.error("indexName="+indexName);
		  IndexStats  indexStats =  indexmapping.get(indexName);
		              //indexStats.getIndex();
		  logger.error("indexStats.getIndex()="+indexStats.getIndex());
		  logger.error("indexStats.getIndex()=");
		  logger.error("indexStats.getIndex()="+indexStats.getIndex());
		  logger.error("indexStats.getIndex()="+indexStats.getIndex());
	}

}




    
}    